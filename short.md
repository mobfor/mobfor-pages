# Installation

Download the provided mobfor.tar docker image, and related scripts (installMobFor.sh, and startMobFor.sh). 

## Install Mobfor

The installMobFor script will install docker, and build the attached docker image. 

Note: you may need to run with sudo. 

```
./installMobFor.sh
```

## Start Mobfor

The startMobFor script will start the generated docker image.
```
./startMobFor.sh
```

<img src="images/startmob.png" width="70%">

<!--


## Generate Docker Image 

### Option 1: Loading From .tar (Recommended)

Download the provided mobfor.tar docker image. 

Load the image by running:
```
docker load -i mobfor.tar
```

You can check that the image was loaded by running:
```
docker images
```

<img src="images/load.png" width="70%">


### Option 2: Building from Dockerfile
<details>
  <summary>Instead of loading the image, you can build your own image</summary>
  

Create a dedicated directory called mobfor. 
```
mkdir mobfor 
cd mobfor
```
Inside the mobfor directory create a file called 'Dockerfile' and populate with the following docker configuration:
```
ARG RUBY_PATH=/usr/local/
ARG RUBY_VERSION=2.6.6

FROM ubuntu:20.04
ARG RUBY_PATH
ARG RUBY_VERSION
ENV PATH $RUBY_PATH/bin:$PATH
ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update && \
    apt-get install -y \
        adb \ 
        aapt \
        openjdk-11-jdk-headless \
        zipalign \
        python3-pip \ 
        wget \
        usbutils \
        git \
        net-tools \
	    curl  \
        ruby-dev \
        gcc \
        make \
        libssl-dev \
        zlib1g-dev \
        libmysqlclient-dev \
        redis-server \
        libsqlite3-dev \
        libreadline-dev \
        build-essential \
        libpq-dev ruby-dev \
	    sqlitebrowser \
        libpcap-dev \
        sqlite3 libsqlite3-dev \
        zip unzip

#COPY --from=rubybuild $RUBY_PATH $RUBY_PATH
#INSTALLING apktool
RUN wget https://bitbucket.org/iBotPeaches/apktool/downloads/apktool_2.4.1.jar && wget https://raw.githubusercontent.com/iBotPeaches/Apktool/master/scripts/linux/apktool && mv apktool_2.4.1.jar apktool.jar
RUN chmod +x apktool* && mv apktool* /usr/bin

RUN git clone git://github.com/rbenv/ruby-build.git $RUBY_PATH/plugins/ruby-build \
&&  $RUBY_PATH/plugins/ruby-build/install.sh

RUN ruby-build $RUBY_VERSION $RUBY_PATH

RUN git clone https://gitlab.com/mobfor/metasploit-framework.git
RUN git clone https://gitlab.com/mobfor/jit-mf.git

RUN cd metasploit-framework/ && gem install bundler &&bundle install
RUN cd ../jit-mf && python3 -m pip install --editable .

# to get JSON export of transaction table
RUN pip3 install sqlite-utils  

ENV PATH /metasploit-framework:$PATH
ENV PATH /metasploit-framework/mobfor:$PATH

WORKDIR "/metasploit-framework/mobfor"

```


In the mobfor directory, run the docker build command:

```
sudo docker build -t mobfor .
```

</details>


## Running Docker Image

Still in the mobfor directory, run the docker container:

```
sudo docker run --rm -it -p 8080:8080 -v ~/.msf4:/root/.msf4 -v ~/.mobfor:/root/.mobfor -v /tmp/locard:/tmp/data --privileged -v /dev/bus/usb:/dev/bus/usb mobfor
```

On windows:

```
docker run --rm -it -p 8080:8080 -v D:\Docker\Mobfor\msf4:/root/.msf4 -v D:\Docker\Mobfor\mobfor:/root/.mobfor -v D:\Docker\Mobfor\data:/tmp/data --privileged -v /dev/bus/usb:/dev/bus/usb mobfor

```
<img src="images/rundocker.png" width="70%">


-->


# Using Mobfor

This is a short version of the documentation. 
For the full version, [click here](https://mobfor.gitlab.io/mobfor-pages/#/full) 

											 
## Running Mock Server 

<details>
  <summary>As a temporary solution, we provided a dummy endpoint that will allow users to test the setup</summary>

  We are currently hosting our own endpoint server http://138.68.85.248:9997/mobfor/api/upload/zip, however, if you wish to run your own instance you can do so by:
  
  * Running the provided endpoint (app.py) on a machine that is reachable by the docker container: 
```
python3 app.py 
```
<img src="images/server.png" width="70%">

  * Modifying the config.ini file in the mobfor directory to reflect the new IP configuration.

<img src="images/config.png" width="50%">

  Note: Make sure that the machine hosting the endpoint is reachable from within the docker container.

</details>

## Connecting Android Device 

Mobfor requires a physical device or emulator (Genymotion) connected through ADB.

### Option 1: Physical Device (Recommended)

Mobfor requires an adb connection to the device. Make sure that:

* The device is connected to the workstation through USB;
* USB debugging is enabled on the device (see: https://developer.android.com/studio/command-line/adb#Enabling);
* Keep an eye on the device and when prompted, confirm the "Allow USB debugging" notification when connecting to a new machine
<img src="images/debugging.png" width="25%">

In the container, run the following to make sure that the docker instance can see the device.

```
adb devices
```

<img src="images/device.png" width="60%">


### Option 2: Emulator
<details>
  <summary>Instead of using a physical device, you may opt to test with an emulator</summary>

We recommend using a Genymotion emulator since this allows you more control over the networking configuration. 
In Genymotion, use the bridged network virtualbox config, since this allows the emulator and Mobfor to run on the same LAN. 

Mobfor requires an adb connection to the emulator. Make sure to:

* Use adb over tcp with an emulator (eg: install wifi adb on emulator https://play.google.com/store/apps/details?id=com.ttxapps.wifiadb&hl=en);
* Enable developer options;
* In developer options, enable "ADB debugging";
* In developer options, disable "verify apps over USB":

<img src="images/verify.png" width="30%">


#### Example: Connecting to genymotion emulator

<details>
  <summary>Below is an example of a genymotion emulator connected to mobfor through wifi adb.</summary>

Install and enable wifi adb on the genymotion emulator:

<img src="images/wifiadb.png" width="30%">

In the docker container type adb connect with the ip address provided on the wifi adb app.
Then run "adb devices" to make sure that the docker instance can see the emulator.

Note: Make sure the emulator network is connected through bridge mode. This will allow the emulator and the docker container to run in the same subnet. 

```
adb connect <emulator_ip>:5555

adb devices
```

<img src="images/emu.png" width="70%">
</details>

</details>

## Running Mobfor

To start running *Mobfor* start by running the mobfor script as shown below and press Enter: 

<img src="images/startmobfor.png" width="70%">

You should now be seeing the Mobfor main menu like the one below:

<img src="images/mobformainmenu.png" width="70%">

## Mobfor - Quick Mode (for testing purposes)

For testing purposes, we have created an option in the main menu called *4. QUICK MODE - GRAB SUSPICIOUS APKS* . This option does the following combined actions:

- Creates a new case and associates a new worklow with it
- Attaches to a device (please make sure there is only one physical device/emulator attached) - using either of the Attach Device options defined above
- Grabs a list of suspicious user-installed apps from the device and stores them in the Mobfor directory hierarchy, locally
- Creates a JSON file with all the *Mobfor internal transactions* carried out
- Creates a zip file of the local Mobfor directory hierarchy, including evidence and trasactions in JSON and stores it in `/root/.mobfor/` in the docker container - see where this directory is mapped locally for easier access
- This zip file is to be uploaded to the BPM, as evidence.
- All cases generated in this "Quick Mode" are automatically named "QM_WF_#" (QuickMode_WorkFlow_<counter>)

### Step 1
Select the option to run in Quick Mode (4)

<img src="images/quickmodestep1.png" width="70%">

### Step 2
Mobfor starts pulling apks from the device

<img src="images/quickmodestep2.png" width="70%">

### Step 3
Mobfor analyses which of the apks are potentially suspicious and stores only those to the `suspicious_apks` directory

<img src="images/quickmodestep3.png" width="70%">

### Step 4
A zip file is generated with the local hierarchy, containing evidence and generated Mobfor transactions in JSON

<img src="images/quickmodestep4.png" width="70%">

### Step 5
Upon pressing "ENTER" in Step 4, you are taken back to the Mobfor Main Menu

<img src="images/quickmodestep5.png" width="70%">

### Step 6
The local directory containing the evidence generated and JSON Mobfor transactions

<img src="images/quickmodestep6.png" width="70%">

### Step 7
<img src="images/quickmodestep7.png" width="70%">
