# MobFor

Mobfor is an Android device message hijack investigation toolkit so that investigators may assess devices for possible unlawful interception attacks. The usage of this toolkit requires collaboration with the owner of the device being investigated. The toolkit consists of 2 main components: a Memory forensics tool for device hijack investigations, and a supporting Android living-off-the-land (LoTL) pentest tool. The pentest tool extends the popular open-source Metasploit Framework, and the memory forensic tool utilises the open-source dynamic binary instrumentation framework, Frida. The figure below shows how the two components interact together. 

![alt_text](images/image1.png "image_tooltip")

Given an Android device from a trusted owner, the pentest tool (stages 1 and 2) allows an investigator to perform a preliminary risk assessment on the given device. This can be done through a set of custom metasploit commands developed for Locard (See appendix A). These stages determine whether or not the device is susceptible to message hijack attacks. If so, the investigator can then proceed with the memory forensic investigation using the memory forensics tool (stages 3 and 4) to forensically enhance the device under investigation and eventually extract relevant memory dumps produced by targeted apps. This toolkit will form part of Locard’s Investigator toolkit. While the current scope of this toolkit is limited to IM and SMS hijack scenarios, it can be extended for further scenarios. 

# Installation

## Environment prerequisites

- We assume that the tool will run on a PC having Ubuntu as it’s OS.
- The tool was tested on an Android 10 Google Pixel emulator and physical Android 10 Google Pixel 3a and app versions Telegram v8.5.4, Whatsapp v2.22.4, Signal v5.32.7 and Pushbullet v18.4
- Create a folder called `MobFor`. Download the contents found when clicking this link: https://tinyurl.com/4kxwh6z9 to obtain the latest tool image, installation and startup scripts which are required for you to get started with the tool. Place the downloaded files in the `MobFor` folder you created.
- For the [reconassaince phase](#stage-1-reconnaissance-amp-stage-2-threat-assessment), any mobile devices that will be analysed should be on the same network as the investigator's machine. This can be achieved by; 1) using a laptop with a Wi-Fi connection that can be used by the mobile device in question, 2) Using a PC that has a Wi-Fi adapter 3) Attaching a router to the PC to which the mobile device can connect to.
- For a full walkthrough on how MobFor can be used, please follow the training slides [here](https://docs.google.com/presentation/d/1J8abe4SNPNLzUfEi2C2JvO0W6fVJmF9QoTIcZhXaDLY/edit?usp=sharing) which include scripts that can be used to generate attacker activity.

### Information Security considerations when installing MobFor:
MobFor is an offline tool; that is, it runs locally on your PC and only trainsmits necessary data externally if the user wishes and in such a way that it abides by the security practices employed by the entity on the receiving-end. Therefore in order to safeguard the use of MobFor it is best to follow the industry standard security practices to secure workstations where MobFor is running. Such practices include the following:
- Lock your PC with a password when it is not in use
- Make sure your firewall is enabled
- Enable or install antivirus protection tools
- Turn on disk encryption

## Intended User and Investigation Scenario
The intended user of this tool is a forensic analyst with expertise in the domain of Android forensics. The tools requires collaboration with the owner of the device being investigated. The content extracted during the investigation process may be of a senstive nature and therefore a formal agreement needs to be signed by both the entity performing the investigation and the device owner, stating that they are both aware of the privacy concerns that may arise.

## Install Mobfor

Go to the `MobFor` directory where you have downloaded the MobFor contents and execute the following command:

```
chmod +x *.sh
```

The installMobFor script will install docker, and load the attached docker image. This script needs to be run *only once* upon installation. 
```
sudo ./installMobFor.sh
```

## Start Mobfor

The startMobFor script will start the generated docker image. You will need to run this script *every time* you intend to start MobFor.
```
sudo  ./startMobFor.sh
```

<img src="images/startmob.png" width="60%">   																		 

## Connecting Android Device 

Mobfor requires a physical device or emulator (Genymotion) connected through ADB.

### Option 1: Physical Device (Recommended)

Mobfor requires an adb connection to the device. Make sure that:

* The device is connected to the workstation through USB;
* The device and the workstation are on the same network;
* USB debugging is enabled on the device (see: https://developer.android.com/studio/command-line/adb#Enabling);
* Keep an eye on the device and when prompted, confirm the "Allow USB debugging" notification when connecting to a new machine

<img src="images/debugging.png" width="25%">

In the container, run the following to make sure that the docker instance can see the device.

```
adb devices
```

<img src="images/device.png" width="50%">


### Option 2: Emulator
<details>
  <summary>Instead of using a physical device, you may opt to test with an emulator</summary>

#### Using Android Studio
Download Android Studio from the link here: https://developer.android.com/studio/index.html then proceed to install an emulator: https://developer.android.com/studio/run/emulator#install. MobFor was tested and works with Android 10 emulators that have Google Play Services enabled.

#### Using Genymotion
In Genymotion, use the bridged network virtualbox config, since this allows the emulator and Mobfor to run on the same LAN. 

Mobfor requires an adb connection to the emulator. Make sure to:

* Use adb over tcp with an emulator (eg: install wifi adb on emulator https://play.google.com/store/apps/details?id=com.ttxapps.wifiadb&hl=en);
* Enable developer options;
* In developer options, enable "ADB debugging";
* In developer options, disable "verify apps over USB":

<img src="images/verify.png" width="30%">


##### Example: Connecting to genymotion emulator

<details>
  <summary>Below is an example of a genymotion emulator connected to mobfor through wifi adb.</summary>

Install and enable wifi adb on the genymotion emulator:

<img src="images/wifiadb.png" width="30%">

In the docker container type adb connect with the ip address provided on the wifi adb app.
Then run "adb devices" to make sure that the docker instance can see the emulator.

Note: Make sure the emulator network is connected through bridge mode. This will allow the emulator and the docker container to run in the same subnet. 

```
adb connect <emulator_ip>:5555

adb devices
```

<img src="images/emu.png" width="70%">
</details>

</details>

## Running Mobfor

To start running *Mobfor* start by running the mobfor script as shown below and press Enter: 

<img src="images/startmobfor.png" width="70%">

You should now be seeing the Mobfor main menu like the one below:

<img src="images/mobformainmenu.png" width="50%">

## Workflow Creation 

### Create Workflow

In the main menu, select option 1 to "CREATE A NEW WORKFLOW":

<img src="images/1.png" width="50%">  

This will ask you to enter a workflow name, and provide the name of the investigator:

<img src="images/2.png" width="50%">   
																											   
From this menu you can open an existing workflow to continue working on it (option 2), view transactions of an existing workflow (option 3), use quick mode (for testing purposes. Read more about this [here](#quick-mode) ) or import a workflow (still a work-in-progress).

All evidence generated/related to Mobfor workflows are stored locally to the container in a directory structure similar to the one below. Furthermore, for every step/option selected/evidence generated/given, a *Mobfor transaction* is generated listing all the high and low level events that occurres as well as the evidence (or a reference to the evidence) generated. Note, every worklow directory is created in `/root/.mobfor/< WORKFLOW_NAME >`.

<img src="images/directory_structure.png" width="30%">

### Attach Device 

Most of Mobfor's feature require an attached device. In the workflow menu, you will see a notification indicating "NO DEVICE ATTACHED". 
To attach a device, select option 1:

<img src="images/3.png" width="40%">\  
																									   
The device attachment is logged:

<img src="images/4.png" width="60%">

## Stage 1: Reconnaissance & Stage 2: Threat Assessment 
																				  
### Start Metasploit Session

Once the device is attached, you can proceed with the recon and threat assessment phase. This part uses the popular metasploit framework, and Mobfor handles the payload creation and pentest server setup. MobFor's risk assessment is intended to assess and Android device’s risk level in terms of accessibility attacks; that is, if an app exposes an accessibility attack vulnerability, is the attack successful on the device under test or is it hardened enough? Note, when selecting this option please make sure that the connected device (or emulator) is freshly configured i.e. there are no existing apps registered with personal accounts that couls sway the assessment.


In the Workflow main menu, select option 2 "START THREAT ASSESSMENT":

<img src="images/5.png" width="50%">

Enter the IP address of your host machine (not the docker container, but the machine hosting the docker container):

<img src="images/6.png" width="40%">

Mobfor will automatically:

* Install target apps (Telegram, Signal, Pushbullet), for which an Accessibility attack is available
* Create a metasploit payload
* Start the metasploit server
* Install the metasploit payload on the attached device

![alt_text](images/7.png "image_tooltip")

Once the metasploit server starts, click on the newly installed app (MainActivity) on your device. This will establish the reverse TCP session.  Note; for this next step to work both the device and the investigator's PC need to be on the **same** network.

<img src="images/MainActivity.png" width="26%">
<img src="images/8.png" width="80%">


Once the reverse TCP session is established, the investigator can now insert metasploit commands. 

### Run Recon Commands

Below are some of the commands that are useful for reconnaissance (see full command list in Annex A): 
*   <strong><code>imei</code></strong>		</strong>Get device IMEI
*   <strong><code>app_list</code></strong>	List installed apps in the device
*   <strong><code>geolocate</code></strong>	Get current lat-long using geolocation
*   <strong><code>check_root</code></strong>	Check if device is rooted
*   <strong><code>sysinfo</code></strong>	Gets information about the remote system, such as OS
*   <strong><code>ps</code></strong>		List running processes
*   <strong><code>ipconfig</code></strong>	Display interfaces

For example, running <strong>imei</strong> will return the device’s International Mobile Equipment Identity (IMEI) number:


<img src="images/9.png" width="80%">


Running the reconnaissance commands will display all of the device’s information on the `msfconsole`. All of the commands and outputs are logged.

### Run Risk Assessment Commands

Before launching the attacks, the investigator must enable accessibility services for the payload:

<img src="images/image8.png" width="80%">

The investigator can now simulate a number of message hijack, and related attacks, available through our custom extension of the Metasploit Framework:
*   **<code>acc_send_sms</code></strong> allows the investigator to send an SMS. This attack opens the default SMS app (<em>com.google.android.apps.messaging</em>), populates the number and message specified as parameters, and finally sends and deletes the message. 

<img src="images/image9.png" width="45%">

*   **<code>acc_send_im</code> </strong>allows the investigator to send an IM on different popular IM apps (Whatsapp: <em>com.whatsapp</em>, Telegram: <em>org.telegram.messenger</em>, and Signal: <em>org.thoughtcrime.securesms</em>). This attack opens the specified IM app specified as the <em>&lt;package_name></em> parameter, populates the number and message specified as parameters, and finally sends and deletes the message.

<img src="images/send_im.png" width="60%">

*   **<code>acc_steal_im</code> </strong>allows the investigator to spy on a specific IM chat. This attack opens the specified IM app specified as the <em>&lt;package_name></em> parameter on the chat specified in the number parameter. The app then parses the content of the page and sends it to the meterpreter output. 

<img src="images/image11.png" width="50%">

<img src="images/image12.png" width="80%">

Besides these message hijack attacks, the tool also contains a number of attacks that can produce more elaborate attack scenarios. For example, the following sequence of steps can be executed in order to obtain an SMS hijack scenario by targeting PushBullet:

*   Force install: <strong><code>acc_install</code></strong> com.pushbullet.android
*   Enable permissions: <strong><code>acc_enable_permissions</code></strong> com.pushbullet.android SMS, Contacts, Phone, Storage
*   Add gmail account to phone:  <strong><code>acc_gmail_account</code></strong> [testaddress@gmail.com](mailto:testaddress@gmail.com) XXXXXXXXXXXX
*   Force login with newly added gmail account: <strong><code>acc_login_pushbullet</code></strong> [testaddress@gmail.com](mailto:testaddress@gmail.com)
*   Send sms from Pushbullet web:  <em>&lt;SMS> &lt;recipient></em> I confess, it was me
*   Delete SMS: <strong><code>acc_delete_sms</code></strong> <em>&lt;recipient></em> "I confess, it was me"

The success or failure of these attacks can prompt the investigator to forensically enhance the device under investigation by targeting apps that are suspected to be victims of a message hijack.  

### Exit Metasploit

Run the command 'exit' twice in order to exit the metasploit session. This will remove the generated payload and the installed apps from the device.


<img src="images/11.png" width="70%">


## Stage 3: Forensic enhancement

The diagram below shows the overall steps that are required in this stage of the toolkit.


![alt_text](images/image13.png "image_tooltip")

1. In the Workflow main menu, select option 3 "GRAB APPS WITH SUSPICIOUS PERMISSIONS", to grab a list of apps containing suspicious permissions. Alternatively if Stages 1 and 2 already highlighted suspicious apps, this step may be skipped.

2. Next, the investigator selects option 4 from the Workflow main menu "START FORENSIC ENHANCEMENT" to forensically enhance the suspicious app.

3. The investigator is given a list of user-installed apps which can be enhanced. The app/s outputted from step 1 should be selected. **Note:** these shoul be "original" apps; i.e. not already forensically enhanced.

![alt_text](images/select_app.png "image_tooltip")

![alt_text](images/select_app2.png "image_tooltip")

4. Next, select the driver you wish to use, depending on the case scenario being investigated. Note, you must select a driver that is associated with the selected app. Choose whether or not to automatically install the app and select the frida version (a default is set).

![alt_text](images/select_driver.png "image_tooltip")

![alt_text](images/select_driver2.png "image_tooltip")

5. Now the forensically enhanced app is generated and installed on the phone (if Y was selected when prompted to install).

Currently the toolkit offer drivers for the four scenarios described below, out of the box:

<table>
  <tr>
   <td>
   </td>
   <td><strong><span style="text-decoration:underline;">App</span></strong>
   </td>
   <td><strong><span style="text-decoration:underline;">Description</span></strong>
   </td>
  </tr>
  <tr>
   <td><strong>Driver 1</strong>
   </td>
   <td>Pushbullet
   </td>
   <td>Driver for investigating messaging hijack scenarios (Crime Proxy and Spying) involving the Pushbullet app
   </td>
  </tr>
  <tr>
   <td><strong>Driver 2</strong>
   </td>
   <td>Telegram
   </td>
   <td>Driver for investigating messaging hijack scenarios (Crime Proxy and Spying) involving the Telegram app
   </td>
  </tr>
  <tr>
   <td><strong>Driver 3</strong>
   </td>
   <td>Signal
   </td>
   <td>Driver for investigating messaging hijack scenarios (Crime Proxy and Spying) involving the Signal app
   </td>
  </tr>
  <tr>
   <td><strong>Driver 4</strong>
   </td>
   <td>Whatsapp
   </td>
   <td>Driver for investigating messaging hijack scenarios (Crime Proxy and Spying) involving the Whatsapp app
   </td>
  </tr>
</table>


This being said, the tool is extensible and supports enhancing any messaging app and scenario, if given a driver. For instance, if an investigator wishes to forensically enhance the Signal app in the case of suspected messaging hijack attack, a driver for that scenario must be compiled and provided as an argument to the forensic tool. Appendix B shows the Frida script generated from Driver 1.

5. The investigator gives the forensically enhanced device back to the owner, for continued normal usage. The user will have to re-login since the app has been re-installed. While using the app, the enhanced app would be generating memory dumps (if events occur) and storing them on external storage.

## Stage 4: Data collection and Analysis

The following options in the Workflow main menu are related to data collection and analysis.

|               Option              |                                                              Description                                                                                                  |
|:--- |:-----:|
| GATHER EVIDENCE                   | Allows investigators to submit additional evidence to the current workflow, for better correlation of events. Gathers any evidence generated by MobFor on the device.|
| ASSEMBLE LOGS                     | Gathers logs/evidence generated from the device. Places them in the workflow directory structure and deletes  evidence from the device itself.|
| ON DEMAND UPLOAD TO LOCARD/SYSTEM | A python client is used to upload the zipped directory structure containing all the evidence generated/found/used in a workflow, as well as all the transactions generated during the course of the workflow. The content is uploaded to a server along with a JSON string (to be used for the chain of custody). The details of the server can be given in a separate configuration file.|

After a period of time that the forensically enhanced app has been deployed, evidence may have accumulated on the device which needs to be gathered and analysed by the investigator. 

1. In the main menu, select the option to "GATHER EVIDENCE". This will obtain any accumulated evidence from the device and ask the investigator for any additional evidence that needs to be added e.g. Call Data Records.

![alt_text](images/gather_evidence.png "image_tooltip")

2. After gathering all the evidence, select the option "ASSEMBLE LOGS" to produce a single zip output file which contains all the generated artifacts, logs as well as high and low level events. This zip file can then be uploaded (so far manually) to the BPM. 

![alt_text](images/assemble_logs.png "image_tooltip")

3. "ON DEMAND UPLOAD TO LOCARD/SYSTEM" allows the user to upload the generated zip file, containing evidence, to the platform in question (in this case LOCARD). MobFor asks for the necessary credentials and case ID, then uploads the the zip file to the platform, together with a hash for the file uploaded.

![alt_text](images/upload_logs.png "image_tooltip")


## Sample Output

The MobFor workflow described above can be replicated using Telegram version 5.0 and a physical device (in this case Google Pixel 5). Following the forensic enhancement of Telegram, a Threat Assessment was performed again and the output obtained from the device is shown below:
![alt_text](images/threat_assessment2.png "image_tooltip")

Artifacts in the zip file:
- `external_resources` - Any additional sources of evidence that are part of this case and may be needed for analysis can be stored here
- `generated_apks` - The forensically enhanced apps that were generated through MobFor are stored here (by MobFor)
- `generated_timelines` - Timelines generated, pertaining to this case, can be stored here
- `logs` - Direct raw output from MobFor terminal (during MobFor's execution) is placed here (by MobFor)
- `original_apks` - The original versions of enhanced apps are stored here (by MobFor)
- `suspicious_apks` - Apps that may misuse accessibility features can be placed here
- `pentest` - Resources used / generated during the risk analysis phase are stored here (by MobFor)
- `jitmf_output` - Any JIT-MF logs assembled (option 6) and gathered from the device are stored here (by MobFor). This folder includes both raw and CSV formatted versions of the JIT-MF logs.
- `wf_id_<id>_transactions.json` - JSON formatted list of MobFor transactions carried out in a workflow (i.e. formatted version of the logs files in the `logs` folder).

![alt_text](images/output1.png "image_tooltip")

Sample list of transactions carried out.

![alt_text](images/output2.png "image_tooltip")

A full list of artifacts generated is shown below. The raw files collected from the device (located in `/raw/*.jitmflog`) are merged and any duplicate logs are removed (`raw/merged-*.jitmflog`). A user-friendly CSV output is then generated and stored in the `/csv` folder.

![alt_text](images/directory_structure.png "image_tooltip")

The CSV output can then be uploaded to tools like [Timesketch](https://timesketch.org/) - by Google -  to generate a forensic timeline similar to the below. For more information on how to use and install Timesketch please consult the manual and guide found here: https://timesketch.org/getting-started/install/.

![alt_text](images/timesketch.png "image_tooltip")

Hijack message sent during threat assessment found in output.

![alt_text](images/output3.png "image_tooltip")

## Future Work and current limitations
* Only one process of a running app can be instrumented at a time - but this can be customized via a meta tag declared in the first line of a driver e.g.: `//[ACTIVITY_OF_INTEREST]=com.pushbullet.android.sms.SendSmsReceiver`

* The tool has only been tested on PCs running Ubuntu. Future full releases will be ported to include other distributions e.g. iOS + WSL (on Windows 10 onwards).

# Testing Guide
<details>

*Starting MobFor and enhancing a messaging app:*

* Make sure your emulator is running / your device is plugged in
* Make sure one of the targeted apps is installed on your device/emulator: Telegram, Signal, WhatsApp and/or Pushbullet. The versions tested are listed at the top of this document
* Make sure MobFor is [installed](https://mobfor.gitlab.io/mobfor-pages/#/?id=install-mobfor) and start [running MobFor](https://mobfor.gitlab.io/mobfor-pages/#/?id=start-mobfor)
* Create a workflow with the case name (workflow description) and your name (investigator name)
* Select the option to start a forensic enhancement process (option 4 on the Menu)
* Select one of the apps available for forensic enhancement, then select the driver
* Opt to reinstall the app
*   Open the app and grant all permissions
*   Register with your sender SIM card number (a number you can access right now)
*   Start a chat with your recipient SIM card number

*Simlulating a messaging hijack attack:*

The following set of testing scripts has been [provided](https://drive.google.com/drive/u/0/folders/1IKlni8Sy5k_7sw-83G7-8lYYW2LJlf0A), to simulate crime-proxy messaging attacks on Telegram, Signal, WhatsApp and Pushbullet:

*   <strong><code>send_delete_telegram_msg.py</code></strong> Sends and automatically deletes a Telegram message
*   <strong><code>send_delete_signal_msg.py</code></strong>	Sends and automatically deletes a Signal message
*   <strong><code>send_delete_whatsapp_msg.py</code></strong> Sends and automatically deletes a WhatsApp message

The above scripts require AndroidViewClient to run, which can be downloaded as per the instructions provided [here](https://github.com/dtmilano/AndroidViewClient/wiki). 
The scripts can then be run by executing: <strong><code>python send_delete_\<app>_msg.py \<message> </code></strong>, where \<app> can be replaced by `telegram`, `signal` or `whatsapp` and \<message> is the message that will be appended to the text saying `Sending_attack_<message>` .

*   <strong><code>send_delete_sms.sh</code></strong> Sends and automatically deletes an SMS that is synced to Pushbullet

The above scripts can be run by rirst executing: <strong><code>chmod +x send_delete_sms.sh</strong></code> to ensure the script is executable. Then <strong><code>./send_delete_sms.sh \<message></code></strong>, where \<message> is the message that will be appended to the text saying `Sending_attack_<message>` .

All the testing scripts provided make the following assumptions:
* An emulator / device is connected
* An active chat is present

*Obtaining elusive evidence through MobFor enhanced app:*

* Open an existing workflow with the case name (workflow description) and your name (investigator name)
* Select options 5, 6 and 7 consecutively to obtain any evidence on the device
* Go to your MobFor store directory:
* <strong><code>sudo -i && cd /root/.mobfor/\<workflow_name>/jitmf_output/ </code></strong>
* Run the following command to check if the attacker’s deleted message was captured: <strong><code>grep -ri "Sending_attack_\<message>"</code></strong> .


</details>

# Appendix A - Meterpreter commands
<details>
  <summary>Meterpreter commands</summary>

Below are all the commands available to the investigator once the payload is installed. Note that the commands in red are the custom commands developed for Locard. 

Core Commands

=============

    Command                   Description

    -------                   -----------

    ?                         Help menu

    background                Backgrounds the current session

    bg                        Alias for background

    bgkill                    Kills a background meterpreter script

    bglist                    Lists running background scripts

    bgrun                     Executes a meterpreter script as a background thread

    channel                   Displays information or control active channels

    close                     Closes a channel

    disable_unicode_encoding  Disables encoding of unicode strings

    enable_unicode_encoding   Enables encoding of unicode strings

    exit                      Terminate the meterpreter session

    get_timeouts              Get the current session timeout values

    guid                      Get the session GUID

    help                      Help menu

    info                      Displays information about a Post module

    irb                       Open an interactive Ruby shell on the current session

    load                      Load one or more meterpreter extensions

    machine_id                Get the MSF ID of the machine attached to the session

    pry                       Open the Pry debugger on the current session

    quit                      Terminate the meterpreter session

    read                      Reads data from a channel

    resource                  Run the commands stored in a file

    run                       Executes a meterpreter script or Post module

    secure                    (Re)Negotiate TLV packet encryption on the session

    sessions                  Quickly switch to another session

    set_timeouts              Set the current session timeout values

    sleep                     Force Meterpreter to go quiet, then re-establish session.

    transport                 Change the current transport mechanism

    use                       Deprecated alias for "load"

    uuid                      Get the UUID for the current session

    write                     Writes data to a channel

Stdapi: File system Commands

============================

    Command       Description

    -------       -----------

    cat           Read the contents of a file to the screen

    cd            Change directory

    checksum      Retrieve the checksum of a file

    cp            Copy source to destination

    dir           List files (alias for ls)

    download      Download a file or directory

    edit          Edit a file

    getlwd        Print local working directory

    getwd         Print working directory

    lcd           Change local working directory

    lls           List local files

    lpwd          Print local working directory

    ls            List files

    mkdir         Make directory

    mv            Move source to destination

    pwd           Print working directory

    rm            Delete the specified file

    rmdir         Remove directory

    search        Search for files

    upload        Upload a file or directory

Stdapi: Networking Commands

===========================

    Command       Description

    -------       -----------

    ifconfig      Display interfaces

    ipconfig      Display interfaces

    portfwd       Forward a local port to a remote service

    route         View and modify the routing table

Stdapi: System Commands

=======================

    Command       Description

    -------       -----------

    execute       Execute a command

    getuid        Get the user that the server is running as

    localtime     Displays the target system's local date and time

    pgrep         Filter processes by name

    ps            List running processes

    shell         Drop into a system command shell

    sysinfo       Gets information about the remote system, such as OS

Stdapi: User interface Commands

===============================

    Command       Description

    -------       -----------

    screenshare   Watch the remote user's desktop in real time

    screenshot    Grab a screenshot of the interactive desktop

Stdapi: Webcam Commands

=======================

    Command        Description

    -------        -----------

    record_mic     Record audio from the default microphone for X seconds

    webcam_chat    Start a video chat

    webcam_list    List webcams

    webcam_snap    Take a snapshot from the specified webcam

    webcam_stream  Play a video stream from the specified webcam

Stdapi: Audio Output Commands

=============================

    Command       Description

    -------       -----------

    play          play an audio file on target system, nothing written on disk

Android Commands

================

    Command                    Description

    -------                    -----------

    acc_default_sms            Set app as default SMS app (Accessibility Attack)

    acc_delete_sms             Delete SMS (Accessibility Attack)

    acc_disable_notifications  Disable Notifications (Accessibility Attack)

    acc_enable_permissions     Enable Permissions (Accessibility Attack)

    acc_gmail_account          Add Gmail Account (Accessibility Attack)

    acc_install                Install App (Accessibility Attack)

    acc_launch_close           Launch and minimise app (Accessibility Attack)

    acc_login_pushbullet       Login Pushbullet (Accessibility Attack)

    acc_login_teamviewer       Login Teamviewer (Accessibility Attack)

    acc_send_im                Send IM (Accessibility Attack)

    acc_send_sms               Send SMS (Accessibility Attack)

    acc_steal_2fa              Steal 2FA from Google authenticator (Accessibility Attack)

    acc_steal_creds            Steal Credentials obtained through overlay attack (Accessibility Attack)

    acc_steal_im               Steal IM (Accessibility Attack)

    acc_uninstall              Uninstall App (Accessibility Attack)

    activity_start             Start an Android activity from a Uri string

    check_root                 Check if device is rooted

    dump_calllog               Get call log

    dump_contacts              Get contacts list

    dump_sms                   Get sms messages

    geolocate                  Get current lat-long using geolocation

    hide_app_icon              Hide the app icon from the launcher

    imei                       Get device IMEI

    interval_collect           Manage interval collection capabilities

    send_sms                   Sends SMS from target session

    set_audio_mode             Set Ringer Mode

    sqlite_query               Query a SQLite database from storage

    wakelock                   Enable/Disable Wakelock

    wlan_geolocate             Get current lat-long using WLAN information

Application Controller Commands

===============================

    Command        Description

    -------        -----------

    app_install    Request to install apk file

    app_list       List installed apps in the device

    app_run        Start Main Activty for package name

    app_uninstall  Request to uninstall application

</details>

# Appendix B - Driver 1 Generated Frida script
<details>
  <summary>Driver 1 Generated Frida script</summary>

  
```
// [ACTIVITY_OF_INTEREST]=com.pushbullet.android.sms.SendSmsReceiver
var LOG_DIR = "jitmflogs";
var APP = "pushbullet";
var TYPE = "native";
var EVENT = "Pushbullet Message Present";
const trigger_points = ["write","read"];// "write" ->  SP trigger point | "read" -> CP trigger point

var jitmfHeapLog = ''
var jitmfLog = ''
var online=true
var complete=false

var OWNER_PHONE_NUMBER = "OWNER"

var Log = Java.use("android.util.Log");
var TAG_L = "[FRIDA_SCRIPT]";

function getTime(file) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = Math.round(today.getTime()/1000);
    if (file) {
        dateTime = date + '_' + time;
    }
    return dateTime;
}
function getExternalStorageDirectory() {
    const Env = Java.use("android.os.Environment")
    var external_storage_dir = Env.getExternalStorageDirectory().getAbsolutePath()
    return external_storage_dir
}

function putInFile(fileName, contents, mode) {
    mode = typeof mode !== 'undefined' ? mode : 'a+';
    try{
        var file = new File(fileName, mode);
    }
    catch (err) { 
        Log.v(TAG_L, "[*] file open err "+err)
    }
    if(mode === 'r'){
        try{
            var file = new File(fileName, mode);
            var br = Java.use("java.io.BufferedReader");
            var fr = Java.use("java.io.FileReader");

            var bufreader = br.$new(fr.$new(fileName)); //expecting a number
            return bufreader.readLine()
        }
        catch (err) { 
            return null; // random number will never be 0
        }
    }
    else{
        var file = new File(fileName, mode);
        file.write(contents)
        file.close()
    }
}

function dumpHeap(TP) {
    var memdump_filename = APP + '_' + TP + "_" + TYPE + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": "' + getTime() + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function parseObject(instance,TP) {
    var json=instance.toString()    
    var object = '';
    var str1_pt1='{"type":"push",'
    var str1_pt2='"push":{"type":"sms_changed","source_device_iden":'    
    var str2_pt1='{"active":'
    var str2_pt2='"message":'
    
    if(json.includes(str1_pt1) && json.includes(str1_pt2) || json.includes(str2_pt1) && json.includes(str2_pt2) ){
        object = json;
    }    
    return object;
}

function getMessageObjects(time,TP) {
	var klass = "org.json.JSONObject"
	Java.choose(klass, {
		onMatch: function (instance) {
            var object = parseObject(instance,TP)
			if (object != '') { 
                jitmfLog += '{"time": "' + time + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": '
                jitmfLog += object + '}\n'
			}

		}, onComplete: function () {
			complete = true
		}
	});
}

function dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP){
    complete = false;
    var time = getTime();
    var hookLog = time + ',' + TP + ',' + TYPE + '\n';

    hookLogName = hookLogName.replace("[TP]",TP);
    jitmfLogName = jitmfLogName.replace("[TP]",TP);
    jitmfHeapLogName = jitmfHeapLogName.replace("[TP]",TP);

    if(online){
        getMessageObjects(time, TP)
    } else {
        dumpHeap(TP)
    }
    if (complete) {
        putInFile(hookLogName, hookLog)
        if(online){
            putInFile(jitmfLogName, jitmfLog)
        } else {
            putInFile(jitmfHeapLogName, jitmfHeapLog)
        }
    }
}

// check for Trigger Point  call
setImmediate(function () {
    Java.perform(function () {
        var hookLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".hooklog";
        var jitmfHeapLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmfheaplog";
        var jitmfLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmflog";
        var predicateFlag = false;
        var samplingFlag = false;
        var dumpFlag = false;
        
        var rndSecond = null;

        var methods = trigger_points
        methods.forEach(function (TP) {
            Interceptor.attach(Module.getExportByName('libc.so', TP), {
                onEnter: function (args) {
                            samplingFlag = false;
                    // predicate for required for both READ and WRITE TP
                    var fd = args[0].toInt32();

                    if (Socket.type(fd) === null)
                        return;
                    if (Socket.type(fd).toString().indexOf('tcp') > -1) {
                        var address = Socket.peerAddress(fd);
                        if (address === null)
                            return;
                        predicateFlag = true
                    }

                    // sampling
                    var d = new Date();
                    var second = d.getSeconds();
                    var minute = d.getMinutes();
                    var totalSeconds = (minute*60) + second;

                    var rndInt = Math.floor(Math.random() * 9) + 1
                    
                    // var value = parseInt(putInFile(tmpFile,"","r"));
            
                    if(rndSecond===null){
                        Log.v(TAG_L,"empty file");
                        rndSecond = rndInt;
                    }
                    else{
                        if(totalSeconds % rndSecond == 0){
                            samplingFlag = true;
                            rndSecond = rndInt;
                        }                    
                    }
                    dumpFlag = samplingFlag && predicateFlag;
                },
                onLeave: function (args) {
                    if (dumpFlag) {
                        dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP)
                    }
                    jitmfHeapLog = ''
                    jitmfLog = ''
                }
            });        
        });    
    });
});
```
</details>																			

# Appendix C - Forensic logs and timelines
<details>
  <summary>Forensic logs and timelines</summary>
The forensically enhanced app generates `*.jitmflog` files when an important event (as defined in the driver) occurs. The below image shows the contents of one particular jitmflog file. 

![alt_text](images/jitmf_content.png "image_tooltip") 

Multiple log files are generated for the duration that the app is being used. Once the device is ready for further forensic analysis and the "EVIDENCE COLLECTION" option (option 5 in the main menu) is selected in the MobFor menu, the logs are merged and unique entries are generated and stored in the `merged*.jitmflog` files as show below. 

![alt_text](images/jitmf_output.png "image_tooltip") 

This step also produces formatted csv output that can be used with tools like [Timesketch](https://timesketch.org/) as demonstrated below, to generate a user-friendly forensic timeline.

## Using Mobfor output in Timesketch

Launch Timesketch and create a new investigation

![alt_text](images/timesketch_1.png "image_tooltip")

Name the timeline

![alt_text](images/timesketch_2.png "image_tooltip")

Choose a file from the `jitmf_output/csv` directory that was generated to populate your timeline

![alt_text](images/timesketch_3.png "image_tooltip")
![alt_text](images/timesketch_4.png "image_tooltip")
![alt_text](images/timesketch_5.png "image_tooltip")

Wait until your timeline is indexed and populated

![alt_text](images/timesketch_6.png "image_tooltip")

Final output from Timesketch should look similar to the below
![alt_text](images/timesketch.png "image_tooltip")

</details>
